package TestLoadingTime;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class UserLoginTime {
	WebDriver driver;

	@Test(priority=1)
	public void urlLoadingTime() {
		WebDriverManager.chromedriver().setup();
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		driver.get("http://172.16.2.152:4201/#/");
	}
	@Test(priority=2)
	public void basicUserLoginTime()
	{
		long startTime = System.currentTimeMillis();
		driver.findElement(By.id("email"));
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys("godfrey.basic@example.com");
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys("1");
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys("2");
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys("3");
		WebElement passcode4 = driver.findElement(By.xpath("// input[@formcontrolname='passcode4']"));
		passcode4.sendKeys("4");

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("Successfull run Test case");
		WebElement display=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-activitymonitoring-page/div/div/form/div/div/div[1]/div[1]/div[1]/div/div[2]/div[1]"));
		System.out.println("-------------------"+display.getText());
		assertEquals("FRONT", display.getText());
		long endTime = System.currentTimeMillis();

		long totalTime = endTime - startTime;

		System.out.println("Total Page Load Time: " + totalTime + " milliseconds");
		driver.close();

	}
}
