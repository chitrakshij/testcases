package com.test;

import org.testng.TestNG;

import com.test.basicuseraismiluatortest.AISimulatorAnomalySendingForCorrectResultTest;
import com.test.basicuseraismiluatortest.AISimulatorNonThreatSendingForInCorrectResultTest;

public class AiSimulatoranomaly {

	static TestNG testNg;

	public static void main(String[] args) {

		testNg = new TestNG();
		testNg.setTestClasses(new Class[] { AISimulatorAnomalySendingForCorrectResultTest.class,AISimulatorNonThreatSendingForInCorrectResultTest.class});

//		AISimulatorNonThreatSendingForCorrectResultTest.class,
//		AISimulatorNonThreatSendingForInCorrectResultTest.class, AISimulatorSendingMultipleThreatTest.class,
//		AISimulatorSendingNonThreatAndAnomalyTest.class, AISimulatorThreadSendingForCorrectResultTest.class,
//		AISimulatorThreadSendingForInCorrectResultTest.class
		//AISimulatorAnomalySendingForInCorrectResultTest.class
		testNg.run();

	}

}
