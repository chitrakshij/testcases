package com.test.advanceusertest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CompleteBetaTestMode {
	WebDriver driver;
	Properties prop;
	WebDriver driver1;
	WebDriver driver2;
	File file;
	@Test(priority=1)
	  public void succesfullyLoginAdvanceUser() throws InterruptedException
	  {
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		System.out.println("Passcode:-****");
		Thread.sleep(5000); 
	  }
	 @Test(priority=2,dependsOnMethods = "succesfullyLoginAdvanceUser")
	  public void clickOnUserSettings() throws InterruptedException
	  {
		  Thread.sleep(1000);
		  JavascriptExecutor executor = (JavascriptExecutor)driver;
		  
		  WebElement nav_btn = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		  executor.executeScript("arguments[0].click()", nav_btn);
		  
		  WebElement user = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[13]/span"));
		  user.click();
	  }
	  
	  @Test(priority=3,dependsOnMethods = "clickOnUserSettings")
	  public void changeBetaTestMode() throws InterruptedException, IOException
	  {
		  String flag;
		  
		  Thread.sleep(1000);
		  JavascriptExecutor executor = (JavascriptExecutor)driver;
	      Thread.sleep(5000);
	      
	      WebElement mode = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[4]/div[2]/div/mat-slide-toggle/label/div/input"));
	      flag = mode.getAttribute("aria-checked");
	      System.out.println("Current Mode:-"+mode.getAttribute("aria-checked"));
	      
	      if(flag.equals("true"))
	      {   
		      System.out.println("Beta Test Mode Is ON");
	    	  
	    	//Login On Basic User
	    	driver2 = new ChromeDriver();
	  		driver2.manage().window().maximize();
	  		Properties prop = new Properties();
	  		FileInputStream ip = new FileInputStream("./config2.properties");
	  		prop.load(ip);

	  		// Deleting all the cookies
	  		driver2.manage().deleteAllCookies();

	  		// Specifiying pageLoadTimeout and Implicit wait
	  		driver2.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
	  		driver2.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

	  		System.out.println("Url :-" + prop.getProperty("URL"));
	  		System.out.println("loading.........");

	  		driver2.get(prop.getProperty("URL"));

	  		WebElement email = driver2.findElement(By.id("email"));
	  		email.sendKeys(prop.getProperty("id"));
	  		System.out.println("Email id:-" + prop.getProperty("id"));

	  		WebElement passcode1 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
	  		passcode1.sendKeys(prop.getProperty("passcode1"));
	  		WebElement passcode2 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
	  		passcode2.sendKeys(prop.getProperty("passcode2"));
	  		WebElement passcode3 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
	  		passcode3.sendKeys(prop.getProperty("passcode3"));
	  		WebElement passcode4 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
	  		passcode4.sendKeys(prop.getProperty("passcode4"));

	  		driver2.findElement(By.className("globalButton")).submit();
	  		System.out.println("Successfully Login On Basic User");
	  		Thread.sleep(4000);
	  		
	  		//Send Threat
	  		driver1 = new ChromeDriver();
			executor = (JavascriptExecutor)driver1;
			driver1.manage().window().maximize();
			
			// Deleting all the cookies
			driver1.manage().deleteAllCookies();

			// Specifiying pageLoadTimeout and Implicit wait
			driver1.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
			driver1.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

			
			driver1.get(prop.getProperty("simulatorUrl"));
			
			
			WebElement left_device_list = driver1.findElement(By.id("mat-select-1"));
			left_device_list.click();
			
			Thread.sleep(1000);


			WebElement left_device_name = driver1
					.findElement(By.xpath(prop.getProperty("Ldevicename")));
			executor.executeScript("arguments[0].click()", left_device_name);
			
			System.out.println("Selected Left Device:-"+ left_device_name.getText());
			
			Thread.sleep(2000);
			WebElement right_device_list = driver1.findElement(By.id("mat-select-2"));
			right_device_list.click();
			
			Thread.sleep(1000);

			WebElement right_device_name = driver1
					.findElement(By.xpath(prop.getProperty("Rdevicename")));
			executor.executeScript("arguments[0].click()", right_device_name);
			System.out.println("Selected Right Device:-"+ right_device_name.getText());

			
			Thread.sleep(2000);

			WebElement anomaly_cb = driver1.findElement(By.xpath(prop.getProperty("SelectAnomalycheckbox")));		
			executor.executeScript("arguments[0].click()", anomaly_cb);

			Thread.sleep(8000);

			WebElement submit = driver1.findElement(By.xpath(
					"/html/body/app-root/app-layout/div/app-threatconfig/form/mat-card/mat-card-content/div/button"));
			submit.submit();
			System.out.println("Anomaly Send Succesfully");
			
			
			//check for log actual result when beta test mode ON

			Thread.sleep(2000);
			System.out.println("----------------------------------------------");

			WebElement threttype = driver1.findElement(By.xpath(prop.getProperty("Anomaly_Name")));
			String send_tt = threttype.getText();
			System.out.println("Sending Threat Type:-" + send_tt);

			WebElement rcvthreat = driver2.findElement(By.xpath(prop.getProperty("RcvThreat_Type")));
			String threattype = rcvthreat.getText();
			System.out.println("Receiving Threat:-" + threattype);

			if (send_tt.contains(threattype)) {

				System.out.println("Test Case Value Matched");
			} else {
				System.out.println("Test case Value does not Match ");
			}
			System.out.println("--------------------------------------------------");
			Date date = new Date();
			@SuppressWarnings("deprecation")
			int hours = date.getHours();
			@SuppressWarnings("deprecation")
			int minutes = date.getMinutes();
			System.out.println("Anomaly Sending Time:-" + hours + ":" + minutes);

			WebElement rcvthreat_time = driver2.findElement(By.xpath(prop.getProperty("RcvThreat_Time")));
			System.out.println("Anomaly Recieving Time :-" + rcvthreat_time.getText());

			String tt[] = rcvthreat_time.getText().split(":");
			int hh = Integer.parseInt(tt[0]);
			int mm = Integer.parseInt(tt[1]);

			if (hours == hh && minutes == mm) {
				System.out.println("Sending and Recieving Time matched");
			} else {
				System.out.println("Sending and Recieving Time does not matched");
			}
			System.out.println("---------------------------------------------------");
			
			Thread.sleep(5000);

			WebElement log = driver2.findElement(By.id("btnLog"));
			log.click();

			WebElement correct_result = driver2.findElement(By.id("r_correct"));
			correct_result.click();

			WebElement confirm_save = driver2.findElement(By.id("btnConfirm"));
			confirm_save.click();
			System.out.println("Successfully Check Log Actual Result Because Beta Test Mode is ON ");
			System.out.println("------------------------------------------------------------------");

			Thread.sleep(1000);
			driver.close();
			driver1.close();
			driver2.close();
	    	  
	      }
	      else
	      {
//	    	  WebElement changebetamode=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[4]/div[2]/div/mat-slide-toggle/label/div/input"));
//	    	  executor.executeScript("arguments[0].click()", changebetamode);
//	    	  System.out.println("Change Into False:-OFF");
	    	  System.out.println("Current Beta Test Mode Is OFF");
	    	  
	    	  //Login On Basic User
	    	driver2 = new ChromeDriver();
	  		driver2.manage().window().maximize();
	  		Properties prop = new Properties();
	  		FileInputStream ip = new FileInputStream("./config2.properties");
	  		prop.load(ip);

	  		// Deleting all the cookies
	  		driver2.manage().deleteAllCookies();

	  		// Specifiying pageLoadTimeout and Implicit wait
	  		driver2.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
	  		driver2.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

	  		System.out.println("Url :-" + prop.getProperty("URL"));
	  		System.out.println("loading.........");

	  		driver2.get(prop.getProperty("URL"));

	  		WebElement email = driver2.findElement(By.id("email"));
	  		email.sendKeys(prop.getProperty("id"));
	  		System.out.println("Email id:-" + prop.getProperty("id"));

	  		WebElement passcode1 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
	  		passcode1.sendKeys(prop.getProperty("passcode1"));
	  		WebElement passcode2 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
	  		passcode2.sendKeys(prop.getProperty("passcode2"));
	  		WebElement passcode3 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
	  		passcode3.sendKeys(prop.getProperty("passcode3"));
	  		WebElement passcode4 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
	  		passcode4.sendKeys(prop.getProperty("passcode4"));

	  		driver2.findElement(By.className("globalButton")).submit();
	  		System.out.println("Successfully Login On Basic User");
	  		Thread.sleep(4000);
	  		
	  		//Send Threat
	  		driver1 = new ChromeDriver();
			executor = (JavascriptExecutor)driver1;
			driver1.manage().window().maximize();
			
			// Deleting all the cookies
			driver1.manage().deleteAllCookies();

			// Specifiying pageLoadTimeout and Implicit wait
			driver1.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
			driver1.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

			
			driver1.get(prop.getProperty("simulatorUrl"));
			
			
			WebElement left_device_list = driver1.findElement(By.id("mat-select-1"));
			left_device_list.click();
			
			Thread.sleep(1000);


			WebElement left_device_name = driver1
					.findElement(By.xpath(prop.getProperty("Ldevicename")));
			executor.executeScript("arguments[0].click()", left_device_name);
			
			System.out.println("Selected Left Device:-"+ left_device_name.getText());
			
			Thread.sleep(2000);
			WebElement right_device_list = driver1.findElement(By.id("mat-select-2"));
			right_device_list.click();
			
			Thread.sleep(1000);

			WebElement right_device_name = driver1
					.findElement(By.xpath(prop.getProperty("Rdevicename")));
			executor.executeScript("arguments[0].click()", right_device_name);
			System.out.println("Selected Right Device:-"+ right_device_name.getText());

			
			Thread.sleep(2000);

			WebElement anomaly_cb = driver1.findElement(By.xpath(prop.getProperty("SelectAnomalycheckbox")));		
			executor.executeScript("arguments[0].click()", anomaly_cb);

			Thread.sleep(8000);

			WebElement submit = driver1.findElement(By.xpath(
					"/html/body/app-root/app-layout/div/app-threatconfig/form/mat-card/mat-card-content/div/button"));
			submit.submit();
			System.out.println("Anomaly Send Succesfully");
			
			
			//check for log actual result when beta test mode ON

			Thread.sleep(2000);
			System.out.println("----------------------------------------------");

			WebElement threttype = driver1.findElement(By.xpath(prop.getProperty("Anomaly_Name")));
			String send_tt = threttype.getText();
			System.out.println("Sending Threat Type:-" + send_tt);

			WebElement rcvthreat = driver2.findElement(By.xpath(prop.getProperty("RcvThreat_Type")));
			String threattype = rcvthreat.getText();
			System.out.println("Receiving Threat:-" + threattype);

			if (send_tt.contains(threattype)) {

				System.out.println("Test Case Value Matched");
			} else {
				System.out.println("Test case Value does not Match ");
			}
			System.out.println("--------------------------------------------------");
			Date date = new Date();
			@SuppressWarnings("deprecation")
			int hours = date.getHours();
			@SuppressWarnings("deprecation")
			int minutes = date.getMinutes();
			System.out.println("Anomaly Sending Time:-" + hours + ":" + minutes);

			WebElement rcvthreat_time = driver2.findElement(By.xpath(prop.getProperty("RcvThreat_Time")));
			System.out.println("Anomaly Recieving Time :-" + rcvthreat_time.getText());

			String tt[] = rcvthreat_time.getText().split(":");
			int hh = Integer.parseInt(tt[0]);
			int mm = Integer.parseInt(tt[1]);

			if (hours == hh && minutes == mm) {
				System.out.println("Sending and Recieving Time matched");
			} else {
				System.out.println("Sending and Recieving Time does not matched");
			}
			System.out.println("---------------------------------------------------");
			System.out.println("Beta Test Mode Is OFF, We Cannot Check Whether the Result is Correct Or Not");
			System.out.println("----------------------------------------------------------------");
	      }
	     
	  }
	@BeforeTest
	  public void beforeTest() throws IOException 
	  {
		WebDriverManager.chromedriver().setup();

		file = new File("config2.properties");
		FileInputStream ip = new FileInputStream(file.getAbsolutePath());

		prop = new Properties();
		prop.load(ip);
			
		ChromeOptions options = new ChromeOptions();
		options.addArguments("headless");

		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
			driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
			
			System.out.println("Url :-" + prop.getProperty("URL"));
			System.out.println("loading.........");
			driver.get(prop.getProperty("URL"));
	  }

	  @AfterSuite
	  public void afterSuite() 
	  {
		  System.out.println("All Test Cases Pass");

	  }
}
