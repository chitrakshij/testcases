package com.test.advanceusertest;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;

public class CompleteDeviceMgmtTest 
{
	WebDriver driver;
	Properties prop;
	@Test(priority=1)
	  public void succesfullyLoginAdvanceUser() throws InterruptedException
	  {
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		Thread.sleep(5000); 
	  }
	@Test(priority=2,dependsOnMethods = "succesfullyLoginAdvanceUser")
	  public void clickOnDeviceMgmt() throws InterruptedException
	  {
		  Thread.sleep(1000);
		  JavascriptExecutor executor = (JavascriptExecutor)driver;

		  WebElement nav_btn = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		  executor.executeScript("arguments[0].click()", nav_btn);
		  WebElement device = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[7]/span"));
		  device.click();
		  System.out.println("-------------------------------------------------------------------");
		  System.out.println("Successfully Click On Device Management");
		  
	  }
	  @Test(priority=3, dependsOnMethods = "clickOnDeviceMgmt")
	  public void successfullyEditDeviceInDeviceMgmt() throws InterruptedException
	  {
		  JavascriptExecutor executor = (JavascriptExecutor)driver;

		  Thread.sleep(1000);
		  WebElement cbox=driver.findElement(By.xpath(prop.getProperty("SelectDevice")));
		  executor.executeScript("arguments[0].click()", cbox);
		  System.out.println("-------------------------------------------------------------------");

		  WebElement devicename=driver.findElement(By.xpath(prop.getProperty("Selectdevicename")));
		  System.out.println("Selected Device:-"+ devicename.getText());
		 
		  WebElement editbutton=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-devicemanagement/div/div/div/div/div/div[3]/div[1]/div[2]/button"));
		  executor.executeScript("arguments[0].click()", editbutton);
		  
		  WebElement editName=driver.findElement(By.id("Name"));
		  editName.clear();
		  editName.sendKeys(prop.getProperty("newName"));
		  
		  WebElement done=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-editdevice/div/div/div/div/div[2]/div/form/div[4]/div[13]/div/div/input"));
		  done.click();
		 
		  System.out.println("New Device Name :-"+ prop.getProperty("newName"));
		  System.out.println("Successfully Edit Device In Device Management");
		  System.out.println("-------------------------------------------------------------------");
		  driver.close();
	  }
	  

  @BeforeTest
  public void beforeTest() throws IOException 
  {
	  System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		prop = new Properties();
		FileInputStream ip=new FileInputStream("./config2.properties");
		prop.load(ip); 
		System.out.println("Url :-" + prop.getProperty("URL"));
		System.out.println("loading.........");
		driver.get(prop.getProperty("URL"));
  }

  @AfterSuite
  public void afterSuite() 
  {
	  System.out.println("All Test Cases Pass");

  }

}
