package com.test.advanceusertest;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;

public class CompleteGateMgmtTest 
{
	WebDriver driver;
	Properties prop;
	@Test(priority=1)
	  public void succesfullyLoginAdvanceUser() throws InterruptedException
	  {
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		Thread.sleep(5000); 
	  }
  @Test(priority=2,dependsOnMethods = "succesfullyLoginAdvanceUser")
  public void clickOnGateMgmt() throws InterruptedException
  {
	  	JavascriptExecutor executor = (JavascriptExecutor)driver;

	  	WebElement nav_btn = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		executor.executeScript("arguments[0].click()", nav_btn);
		
		WebElement gate = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[5]"));
		gate.click();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Successfully Click On Gate Management Menu");
		Thread.sleep(2000);
  }
  @Test(priority=3, dependsOnMethods = "clickOnGateMgmt")
  public void successfullyAddGateInGateManagement() throws InterruptedException
  {
		Thread.sleep(2000);
		WebElement Agate = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-hexwavetogate/div/div/div/div/div[1]/div[3]/div[1]/div[2]/div/div[3]/button"));
		Agate.click();
		WebElement Gname = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/mat-dialog-container/app-addgate/div/div/form/div/div[1]/div[2]/div/input"));
		Gname.sendKeys(prop.getProperty("gateName"));
		WebElement Add = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/mat-dialog-container/app-addgate/div/div/form/div/div[2]/div[2]/input"));
		Add.submit();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Succesfully Add gate:-"+ prop.getProperty("gateName"));
		System.out.println("Successfully Add Gate In Gate Management");
		Thread.sleep(2000);

  }
  @Test(priority=4,dependsOnMethods = "clickOnGateMgmt")
  public void successfullydeleteGateInGateManagement() throws InterruptedException 
  {
	  	Thread.sleep(1000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		WebElement cbox=driver.findElement(By.xpath(prop.getProperty("SelectGatexpath")));
		executor.executeScript("arguments[0].click()", cbox);
		WebElement dgate=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-hexwavetogate/div/div/div/div/div[1]/div[3]/div[1]/div[2]/div/div[2]/button"));
		executor.executeScript("arguments[0].click()", dgate);
		WebElement Smit=driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/mat-dialog-container/app-alert-component/div/div[2]/button"));
		executor.executeScript("arguments[0].click()", Smit);
		Thread.sleep(2000);
		WebElement close=driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/mat-dialog-container/app-alert-component/div/div[1]/div[3]/button"));
		close.click();
		WebElement dltgatename=driver.findElement(By.xpath(prop.getProperty("SelectGateName")));
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Delete Gate :-"+ dltgatename.getText());
		System.out.println("Successfully Delete Gate In Gate Management");
		Thread.sleep(2000);

  }
  @Test(priority=5,dependsOnMethods = "clickOnGateMgmt")
  public void successfullyClickOnMapView() throws InterruptedException
  {
	  Thread.sleep(1000);
	  WebElement MapView=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-hexwavetogate/div/div/div/div/div[1]/div[3]/div[1]/div[2]/div/div[1]/button"));
	  MapView.click();
	  System.out.println("-------------------------------------------------------------------");
	  System.out.println("Successfully Click On Map View");
	  System.out.println("-------------------------------------------------------------------");
	  driver.close();
  }
  @BeforeTest
  public void beforeTest() throws IOException 
  {
	  System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		prop = new Properties();
		FileInputStream ip=new FileInputStream("./config2.properties");
		prop.load(ip); 
		System.out.println("Url :-" + prop.getProperty("URL"));
		System.out.println("loading.........");
		driver.get(prop.getProperty("URL"));
  }

  @AfterSuite
  public void afterSuite() 
  {
	  System.out.println("All Test Cases Pass");

  }

}
