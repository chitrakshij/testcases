package com.test.advanceusertest;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterSuite;

public class CompleteTabletMgmtTest 
{
	WebDriver driver;
	Properties prop;
	File file;
	@Test(priority=1)
	  public void succesfullyLoginAdvanceUser() throws InterruptedException
	  {
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		System.out.println("Passcode:-****");
		Thread.sleep(5000); 
	  }
	 @Test(priority=2,dependsOnMethods = "succesfullyLoginAdvanceUser")
	  public void clickOnTabletManagement() throws InterruptedException
	  {
		  Thread.sleep(1000);
		  JavascriptExecutor executor = (JavascriptExecutor)driver;
		  
		  WebElement nav_btn = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		  executor.executeScript("arguments[0].click()", nav_btn);
		  
		  WebElement tablet = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[8]/span"));
		  tablet.click();
		  System.out.println("-------------------------------------------------------------------");
		  System.out.println("Successfully Click On Tablet Management");
		  Thread.sleep(2000);
		  
	  }
	  @Test(priority=3,dependsOnMethods = "clickOnTabletManagement")
	  public void successfullyCreateTabletInTabletMgmt() throws InterruptedException
	  {
		  	JavascriptExecutor executor = (JavascriptExecutor)driver;
		  	Thread.sleep(1000);
		  	
		  	WebElement ctablet = driver.findElement(By.xpath("//BUTTON[@class='globalButton'][text()='CREATE TABLET']"));
			executor.executeScript("arguments[0].click()", ctablet);
						
			WebElement Tname = driver.findElement(By.xpath("//INPUT[@id='tabletName']"));
			Tname.sendKeys(prop.getProperty("TabletName"));
			System.out.println("-------------------------------------------------------------------");
			
			System.out.println("Create Tablet :="+ prop.getProperty("TabletName"));
			
			WebElement Tmadd = driver.findElement(By.xpath("//INPUT[@id='tabletMacAddress']"));
			Tmadd.sendKeys(prop.getProperty("MACAddress"));
			
			System.out.println("MAC Address:-"+ prop.getProperty("MACAddress"));
			
			Select Tstatus = new Select(driver.findElement(By.xpath("//SELECT[@id='tabletStatus']")));
			Tstatus.selectByValue("true");
			
			WebElement Smit=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-addtablet/div/div/div/div/div[2]/div/form/div[4]/div[9]/div/div/input"));
			executor.executeScript("arguments[0].click()", Smit);
			System.out.println("Successfully Create Tablet In Tablet Management");
			Thread.sleep(1000);
	  }
	  
	  @Test(priority=4,dependsOnMethods = "clickOnTabletManagement")
	  public void succesfullyEditTabletInTabletMgmt() throws InterruptedException
	  {
		  	Thread.sleep(1000);
		  	JavascriptExecutor executor = (JavascriptExecutor)driver;
		  	Thread.sleep(4000);
		  	List<WebElement> cboxNo=driver.findElements(By.xpath("//table[@class='table table-stripes mat-table']//tr"));
			//System.out.println(cboxNo.size());
			int seq=cboxNo.size();
			int seqno=--seq;
			//System.out.println(seqno);
			String path1 = "/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container\n"
					+ "/mat-sidenav-content/div/app-tablet/div/div/div/div/div/div[3]/div[2]\n"
					+ "/table/tbody/tr[";
			String path2 = "]/td[1]/mat-checkbox/label/div";
			WebElement cbox=driver.findElement(By.xpath(path1.concat(Integer.toString(seqno)).concat(path2)));
			executor.executeScript("arguments[0].click()", cbox);

			System.out.println("-------------------------------------------------------------------");
			
			WebElement etab=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-tablet/div/div/div/div/div/div[3]/div[1]/div[2]/div/div[1]/button"));
			executor.executeScript("arguments[0].click()", etab);
			
			WebElement editName=driver.findElement(By.id("tabletName"));
			editName.clear();
			editName.sendKeys(prop.getProperty("NewTabletName"));
			System.out.println("New Name of Tablet:-"+ prop.getProperty("NewTabletName"));

			Select Tstatus = new Select(driver.findElement(By.xpath("//SELECT[@id='tabletStatus']")));
			Tstatus.selectByValue("true");
			
			WebElement Smit=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-edittablet/div/div/div/div/div[2]/div/form/div[4]/div[9]/div/div/input"));
			executor.executeScript("arguments[0].click()", Smit);
			System.out.println("Succesfully Edit Tablet In Tablet Management");
			Thread.sleep(2000);
	  }
	  @Test(priority=5,dependsOnMethods = "clickOnTabletManagement")
	  public void succesfullyDeleteTabletInTabletMgmt() throws InterruptedException
	  {
		  Thread.sleep(1000);
		  	JavascriptExecutor executor = (JavascriptExecutor)driver;
		  	Thread.sleep(4000);
		  	List<WebElement> cboxNo=driver.findElements(By.xpath("//table[@class='table table-stripes mat-table']//tr"));
			//System.out.println(cboxNo.size());
			int seq=cboxNo.size();
			int seqno=--seq;
			//System.out.println(seqno);
			String path1 = "/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container\n"
					+ "/mat-sidenav-content/div/app-tablet/div/div/div/div/div/div[3]/div[2]\n"
					+ "/table/tbody/tr[";
			String path2 = "]/td[1]/mat-checkbox/label/div";
			WebElement cbox=driver.findElement(By.xpath(path1.concat(Integer.toString(seqno)).concat(path2)));
			executor.executeScript("arguments[0].click()", cbox);

			System.out.println("-------------------------------------------------------------------");
			
			System.out.println("Delete Tablet Name is:-"+ prop.getProperty("NewTabletName"));
			WebElement dtab=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-tablet/div/div/div/div/div/div[3]/div[1]/div[2]/div/div[3]/button"));
			executor.executeScript("arguments[0].click()", dtab);

			
			WebElement Smit=driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/mat-dialog-container/app-alert-component/div/div[2]/button"));
			executor.executeScript("arguments[0].click()", Smit);
			Thread.sleep(5000);
			
			WebElement ok=driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/mat-dialog-container/app-alert-component/div/div[1]/div[3]/button"));
			executor.executeScript("arguments[0].click()", ok );
			System.out.println("Succesfully Delete Tablet In Tablet Management");
			System.out.println("-------------------------------------------------------------------");
			driver.close();
	  }
	  @BeforeTest
		public void beforeTest() throws IOException 
		{

			WebDriverManager.chromedriver().setup();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("headless");
			driver = new ChromeDriver(options);
			driver.manage().window().maximize();
			driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

			file = new File("config2.properties");
			FileInputStream ip = new FileInputStream(file.getAbsolutePath());

			prop = new Properties();
			prop.load(ip);

			driver.get(prop.getProperty("URL"));
			System.out.println("Url :-" + prop.getProperty("URL"));
		}

		@AfterSuite
		public void afterSuite()
		{
			driver.close();
			System.out.println("All Test Cases Pass");

		}

}
