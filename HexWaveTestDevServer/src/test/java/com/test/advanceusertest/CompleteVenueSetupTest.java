package com.test.advanceusertest;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CompleteVenueSetupTest 
{
	WebDriver driver;
	Properties prop;
	 @Test(priority=1)
	  public void succesfullyLoginAdvanceUser() throws InterruptedException
	  {
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		Thread.sleep(5000); 
	  }
	 @Test(priority=2,dependsOnMethods = "succesfullyLoginAdvanceUser")
	  public void venueSetupTest()
	  {
		  	WebElement nav_btn = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
			nav_btn.click();
			
			WebElement venue = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[4]"));
			venue.click();
		
	  }
	  @Test(priority=3,dependsOnMethods = "venueSetupTest")
	  public void succesffullyUploadLogoInVenuSetup() throws Throwable 
	  {
		  	JavascriptExecutor executor=(JavascriptExecutor) driver;
			Robot robot = new Robot();

			WebElement logo_file = driver.findElement(By.xpath(
					"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-venuesetup/div/div/div[1]/div[1]/div[2]/div[2]/div[2]/div/div[2]/div[1]/button"));
			executor.executeScript("arguments[0].click()", logo_file);
			Thread.sleep(5000);

			StringSelection logo_image = new StringSelection("E:\\dsh\\logo.JPG");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(logo_image, null);

			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);

			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);

			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(2000);
			WebElement done=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-venuesetup/div/div/div[2]/div/button"));
			executor.executeScript("arguments[0].click()", done);
			
			Thread.sleep(2000);
			WebElement close=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-installationtype/div/div[1]/div[3]/div[1]/div[1]/div[1]/div/label"));
			close.click();
			
			WebElement exit=driver.findElement(By.id("iddone"));
			exit.click();
			System.out.println("-------------------------------------------------------------------");

			System.out.println("Succesffully Upload Logo In Venue Setup");
			Thread.sleep(2000);
	  }

	  @Test(priority=4, dependsOnMethods = "venueSetupTest")
	  public void successfullyUploadFootprintInVenuSetup() throws AWTException, InterruptedException
	  {
		  	JavascriptExecutor executor = (JavascriptExecutor)driver;

		  	WebElement nav_btn = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
			executor.executeScript("arguments[0].click()", nav_btn);
			
			WebElement venue = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[4]"));
			venue.click();
			
			Robot robot = new Robot();

			WebElement Footprint_file = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-venuesetup/div/div/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/button"));
			executor.executeScript("arguments[0].click()", Footprint_file);
			Thread.sleep(5000);

			StringSelection Footprint_image = new StringSelection("E:\\dsh\\footprint.JPG");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(Footprint_image, null);

			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);

			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);

			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(2000);

			WebElement done=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-venuesetup/div/div/div[2]/div/button"));
			executor.executeScript("arguments[0].click()", done);
			
			Thread.sleep(2000);
			WebElement close=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-installationtype/div/div[1]/div[3]/div[1]/div[1]/div[1]/div/label"));
			close.click();
			
			WebElement exit=driver.findElement(By.id("iddone"));
			exit.click();
			System.out.println("-------------------------------------------------------------------");

			System.out.println("Succesffully Upload Footprint In Venue Setup");
			Thread.sleep(3000);
		}
	  @Test(priority=5, dependsOnMethods = "venueSetupTest")
	  public void successfullyRevertLogoInVenueSetup() throws AWTException, InterruptedException
	  {
		  	JavascriptExecutor executor = (JavascriptExecutor)driver;

		  	WebElement nav_btn = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
			executor.executeScript("arguments[0].click()", nav_btn);
			
			WebElement venue = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[4]"));
			venue.click();
			
			Robot robot = new Robot();

			WebElement Rvert_file = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-venuesetup/div/div/div[1]/div[1]/div[2]/div[2]/div[2]/div/div[2]/div[1]/button"));
			executor.executeScript("arguments[0].click()", Rvert_file);
			Thread.sleep(5000);

			StringSelection Footprint_image = new StringSelection("E:\\dsh\\revertlogo.jpg");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(Footprint_image, null);

			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);

			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);

			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(2000);

			WebElement revert=driver.findElement(By.id("revert_logo"));
			revert.click();
			System.out.println("-------------------------------------------------------------------");

			System.out.println("Succesffully Revert Logo In Venue Setup");
			Thread.sleep(3000);
	  }
	  @Test(priority=6, dependsOnMethods = "venueSetupTest")
	  public void successfullyRevertFootprintInVenueSetup() throws AWTException, InterruptedException
	  {
		  	JavascriptExecutor executor = (JavascriptExecutor)driver;

		  	WebElement nav_btn = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
			executor.executeScript("arguments[0].click()", nav_btn);
			
			WebElement venue = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[4]"));
			venue.click();
			
			Robot robot = new Robot();

			WebElement Rvert_file = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-venuesetup/div/div/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/button"));
			executor.executeScript("arguments[0].click()", Rvert_file);
			Thread.sleep(5000);

			StringSelection Footprint_image = new StringSelection("E:\\dsh\\revertlogo.jpg");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(Footprint_image, null);

			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);

			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);

			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(2000);

			WebElement revert=driver.findElement(By.id("clear"));
			revert.click();
			System.out.println("-------------------------------------------------------------------");

			System.out.println("Succesffully Revert FootPrint In Venue Setup");
			System.out.println("-------------------------------------------------------------------");

			Thread.sleep(3000);
			driver.close();
			
	  }
  @BeforeTest
  public void beforeTest() throws IOException 
  {
	  	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		prop = new Properties();
		FileInputStream ip=new FileInputStream("./config2.properties");
		prop.load(ip); 
		System.out.println("Url :-" + prop.getProperty("URL"));
		System.out.println("loading.........");
		driver.get(prop.getProperty("URL"));
		
		
  }

  @AfterSuite
  public void afterSuite()
  {
	  System.out.println("All Test Cases Pass");
  }
}
