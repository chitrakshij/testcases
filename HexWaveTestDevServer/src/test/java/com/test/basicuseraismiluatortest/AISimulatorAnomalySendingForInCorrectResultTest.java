package com.test.basicuseraismiluatortest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

@Listeners(com.test.TestListener.class)

public class AISimulatorAnomalySendingForInCorrectResultTest {
	WebDriver driver;
	WebDriver driver2;
	Properties prop;
	File file;
	Connection con;
	JavascriptExecutor executor;

	@Test(priority = 1)
	public void basicUserLoginMethod() throws Throwable {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("headless");
		
		driver2 = new ChromeDriver(options);
		driver2.manage().window().maximize();
		driver2.manage().window().maximize();

		// Deleting all the cookies
		driver2.manage().deleteAllCookies();

		// Specifiying pageLoadTimeout and Implicit wait
		driver2.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver2.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

		System.out.println("Url :-" + prop.getProperty("URL"));
		System.out.println("loading.........");

		driver2.get(prop.getProperty("URL"));

		WebElement email = driver2.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("id"));
		System.out.println("Email id:-" + prop.getProperty("id"));

		WebElement passcode1 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver2.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver2.findElement(By.className("globalButton")).submit();
		System.out.println("Successfully Login On Basic User");
		Thread.sleep(4000);
	}

	@Test(priority = 2, dependsOnMethods = "basicUserLoginMethod")
	public void succesfullySendAnomaly() throws Throwable {
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("headless");

		driver = new ChromeDriver(options);
		executor = (JavascriptExecutor) driver;
		driver.manage().window().maximize();

		// Deleting all the cookies
		driver.manage().deleteAllCookies();

		// Specifiying pageLoadTimeout and Implicit wait
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

		driver.get(prop.getProperty("simulatorUrl"));

		WebElement left_device_list = driver.findElement(By.id("mat-select-1"));
		left_device_list.click();

		Thread.sleep(1000);

		WebElement left_device_name = driver.findElement(By.xpath(prop.getProperty("Ldevicename")));
		executor.executeScript("arguments[0].click()", left_device_name);

		System.out.println("Selected Left Device:-" + left_device_name.getText());

		Thread.sleep(2000);
		WebElement right_device_list = driver.findElement(By.id("mat-select-2"));
		right_device_list.click();

		Thread.sleep(1000);

		WebElement right_device_name = driver.findElement(By.xpath(prop.getProperty("Rdevicename")));
		executor.executeScript("arguments[0].click()", right_device_name);
		System.out.println("Selected Right Device:-" + right_device_name.getText());

		Thread.sleep(2000);

		WebElement anomaly_cb = driver.findElement(By.xpath(prop.getProperty("SelectAnomalycheckbox")));
		executor.executeScript("arguments[0].click()", anomaly_cb);

//		WebElement anomaly_name=driver.findElement(By.xpath(prop.getProperty("Anomaly_Name")));
//		System.out.println("Sending Threat Type:-"+ anomaly_name.getText());

		Thread.sleep(8000);

		WebElement submit = driver.findElement(By.xpath(
				"/html/body/app-root/app-layout/div/app-threatconfig/form/mat-card/mat-card-content/div/button"));
		submit.submit();
		System.out.println("Anomaly Send Succesfully");
		System.out.println("Successfully pass:-SuccesfullySendAnomaly");

	}

	@Test(priority = 3, dependsOnMethods = "succesfullySendAnomaly")
	public void anomalyCorrectResult() throws IOException, InterruptedException {

		executor = (JavascriptExecutor) driver2;

		Thread.sleep(2000);
		System.out.println("----------------------------------------------");

		WebElement threttype = driver.findElement(By.xpath(prop.getProperty("Anomaly_Name")));
		String send_tt = threttype.getText();
		System.out.println("Sending Threat Type:-" + send_tt);

		WebElement rcvthreat = driver2.findElement(By.xpath(prop.getProperty("RcvThreat_Type")));
		String threattype = rcvthreat.getText();
		System.out.println("Receiving Threat:-" + threattype);

		if (send_tt.contains(threattype)) {

			System.out.println("Test Case Value Matched");
		} else {
			System.out.println("Test case Value does not Match ");
		}
		System.out.println("--------------------------------------------------");
		Date date = new Date();
		@SuppressWarnings("deprecation")
		int hours = date.getHours();
		@SuppressWarnings("deprecation")
		int minutes = date.getMinutes();
		System.out.println("Anomaly Sending Time:-" + hours + ":" + minutes);

		WebElement rcvthreat_time = driver2.findElement(By.xpath(prop.getProperty("RcvThreat_Time")));
		System.out.println("Anomaly Recieving Time :-" + rcvthreat_time.getText());

		String tt[] = rcvthreat_time.getText().split(":");
		int hh = Integer.parseInt(tt[0]);
		int mm = Integer.parseInt(tt[1]);

		if (hours == hh && minutes == mm) {
			System.out.println("Sending and Recieving Time matched");
		} else {
			System.out.println("Sending and Recieving Time does not matched");
		}
		System.out.println("--------------------------------------------------");
		System.out.println("But the Threat We Getting is Incorrect");
		System.out.println("---------------------------------------------------");
		Thread.sleep(5000);

		WebElement log = driver2.findElement(By.id("btnLog"));
		executor.executeScript("arguments[0].click()", log);

		WebElement incorrect_result = driver2.findElement(By.id("r_incorrect"));
		executor.executeScript("arguments[0].click()", incorrect_result);

		WebElement threatType = driver2.findElement(By.id("threatType"));
		Select threat_Type = new Select(threatType);
		threat_Type.selectByValue("Threat");

		System.out.println("And the expected Result is:-" + prop.getProperty("ExpectedResult"));

		Thread.sleep(1000);

		WebElement weaponType = driver2.findElement(By.id("weaponType"));
		Select weapon_Type = new Select(weaponType);
		weapon_Type.selectByValue("Rifle");
		System.out.println("Type Of Weapon:-" + prop.getProperty("WeaponType"));

		Thread.sleep(1000);

		WebElement threatLocation = driver2.findElement(By.id("threatLocation"));
		Select threat_Location = new Select(threatLocation);
		threat_Location.selectByValue("Left chest front");
		System.out.println("Location Of Weapon:-" + prop.getProperty("Loaction"));

		Thread.sleep(1000);

		WebElement btnContinue = driver2.findElement(By.id("btnContinue"));
		executor.executeScript("arguments[0].click()", btnContinue);

		WebElement textarea = driver2.findElement(By.id("name"));

		textarea.sendKeys("all the given results are correct");

		Thread.sleep(1000);

		WebElement closeBtn = driver2.findElement(By.className("addnote_header_btnclose"));
		executor.executeScript("arguments[0].click()", closeBtn);

		Thread.sleep(2000);

		WebElement confirm_save = driver2.findElement(By.id("btnConfirm"));
		confirm_save.click();
		System.out.println("--------------------------------------------------");
		System.out.println("Succesfully Check For incorrect Result");
		System.out.println("--------------------------------------------------");
		Thread.sleep(5000);

		WebElement logdetail = driver2.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-activitymonitoring-page/div/div/form/div/div/div[2]/div[2]/div[1]/div/mat-table/mat-row[1]/td[3]/div"));
		executor.executeScript("arguments[0].click()", logdetail);
		Thread.sleep(2000);
		WebElement result = driver2.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div/mat-dialog-container/app-logdetails/div/div[2]/div/div/div[1]/div/div/div[1]"));
		System.out.println("Threat Result=" + result.getText());
		// executor.executeScript("Threat Result="+result.getText());
		Thread.sleep(2000);

		WebElement threatlog = driver2.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div/mat-dialog-container/app-logdetails/div/div[2]/div/div/div[2]/div[2]/div/div/div[1]"));
		System.out.println("Actual Threat=" + threatlog.getText());
		Thread.sleep(2000);

		WebElement actuallog = driver2.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div/mat-dialog-container/app-logdetails/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]"));
		System.out.println("logged Threat=" + actuallog.getText());
		Thread.sleep(2000);

		WebElement weatype = driver2.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div/mat-dialog-container/app-logdetails/div/div[2]/div/div/div[2]/div[3]/div/div/div[2]"));
		System.out.println("Weapon type=" + weatype.getText());
		Thread.sleep(2000);
		WebElement locType = driver2.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div/mat-dialog-container/app-logdetails/div/div[2]/div/div/div[2]/div[4]/div/div/div[2]"));
		System.out.println("Logged Location of weapon= " + locType.getText());
		Thread.sleep(2000);
		WebElement Loggednote = driver2.findElement(By.xpath(
				"/html/body/div[3]/div[2]/div/mat-dialog-container/app-logdetails/div/div[2]/div/div/div[3]/div/div/div"));
		System.out.println("Add Note=" + Loggednote.getText());
		driver.close();
		driver2.close();

	}

	@Test(priority = 4)
	public void threatVerifyFromDB() throws Exception {

		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM  threat_logs_report ORDER BY threat_config_id DESC LIMIT 1");
		while (rs.next()) {
			System.out.println("DataBase Value are:- ");
			System.out.println("Threat sending time Check from DB:-" + rs.getTimestamp("creation_date"));

			if (rs.getString("config_weapon_type") == null) {
				// System.out.println("config_weapon_type is haven't any value");
			} else {
				System.out.println("Threat Type From DB" + rs.getString("config_weapon_type"));
			}
			if (rs.getString("config_threat_location") == null) {
				// System.out.println("config_weapon_type is haven't any value");
			} else {
				System.out.println("Threat Location In DB" + rs.getString("config_threat_location"));
			}
			if (rs.getString("config_threat_type") == null) {
				// System.out.println("config_weapon_type is haven't any value");
			} else {
				System.out.println(
						"Type of Threat,Non-Threat or anomaly sending:- " + rs.getString("config_threat_type"));
			}

		}

		System.out.println("----------------------------------------------------");
	}

	@BeforeTest
	public void beforeTest() throws IOException, Exception {
		WebDriverManager.chromedriver().setup();

		file = new File("config2.properties");
		FileInputStream ip = new FileInputStream(file.getAbsolutePath());

		prop = new Properties();
		prop.load(ip);
		Class.forName("com.mysql.cj.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://104.198.155.142:3306/hexwave?createDatabaseIfNotExist=true",
				"hexwave", "h3xW@veDevAugust13");
	}

	@AfterSuite
	public void aftersuit() {
		System.out.println("All Test Cases Pass Successfully");
	}
}
