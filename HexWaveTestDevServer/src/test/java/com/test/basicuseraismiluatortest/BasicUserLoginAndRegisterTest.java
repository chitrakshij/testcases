package com.test.basicuseraismiluatortest;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
@Listeners(com.test.TestListener.class)

public class BasicUserLoginAndRegisterTest {
	WebDriver driver;
	Properties prop;
	File file;
	Connection con;

	@Test(priority = 1)
	public void successfullyRegisterBasicUser() throws IOException, InterruptedException {
//		Properties prop = new Properties();
//		FileInputStream ip = new FileInputStream("./config2.properties");
//		prop.load(ip);
		System.out.println("Url :-" + prop.getProperty("URL"));
		System.out.println("loading.......................");
		System.out.println("---------------------------------------------------");
		driver.get(prop.getProperty("URL"));
		WebElement register = driver.findElement(By.xpath(
				"/html/body/app-root/app-login/div[2]/div/div/div/div/form/div[2]/div/div[2]/div[7]/span/strong"));
		register.click();
		WebElement fname = driver.findElement(By.id("firstName"));
		fname.sendKeys(prop.getProperty("FirstNameBasic"));
		System.out.println("First Name:-" + prop.getProperty("FirstNameBasic"));
		WebElement lname = driver.findElement(By.xpath("//input[@formcontrolname='lastName']"));
		lname.sendKeys(prop.getProperty("LastNameBasic"));
		System.out.println("Last Name:-" + prop.getProperty("LastNameBasic"));
		WebElement email = driver.findElement(By.xpath("//input[@formcontrolname='email']"));
		email.sendKeys(prop.getProperty("MailIdBasic"));
		System.out.println("EmailId:-" + prop.getProperty("MailIdBasic"));
		WebElement confirmEmail = driver.findElement(By.xpath("//input[@formcontrolname='confirmEmail']"));
		confirmEmail.sendKeys(prop.getProperty("MailIdBasic"));
		System.out.println("Confrim EmailId:-" + prop.getProperty("MailIdBasic"));

		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys("1");
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys("2");
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys("3");
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys("4");
		System.out.println("Passcode:-" + "****");

		WebElement confpasscode1 = driver.findElement(By.xpath("//input[@formcontrolname='confPasscode1']"));
		confpasscode1.sendKeys("1");
		WebElement confpasscode2 = driver.findElement(By.xpath("//input[@formcontrolname='confPasscode2']"));
		confpasscode2.sendKeys("2");
		WebElement confpasscode3 = driver.findElement(By.xpath("//input[@formcontrolname='confPasscode3']"));
		confpasscode3.sendKeys("3");
		WebElement confpasscode4 = driver.findElement(By.xpath("//input[@formcontrolname='confPasscode4']"));
		confpasscode4.sendKeys("4");

		System.out.println("Confirm Passcode:-****");
		driver.findElement(By.className("globalButton")).submit();

		driver.findElement(By.id("mat-checkbox-1")).click();
		driver.findElement(By.className("globalButton")).submit();
		System.out.println("Successfully Register Basic User");
		Thread.sleep(2000);

	}

	@Test(priority = 2)
	public void successfullyCheckAlreayHaveAnAccount() throws IOException, InterruptedException
	{
		Thread.sleep(2000);

//		Properties prop = new Properties();
//		FileInputStream ip = new FileInputStream("./config2.properties");
//		prop.load(ip);
		System.out.println("Url :-" + prop.getProperty("URL"));
		System.out.println("loading.......................");
		System.out.println("---------------------------------------------------");
		driver.get(prop.getProperty("URL"));
		WebElement register = driver.findElement(By.xpath(
				"/html/body/app-root/app-login/div[2]/div/div/div/div/form/div[2]/div/div[2]/div[7]/span/strong"));
		register.click();
		WebElement fname = driver.findElement(By.id("firstName"));
		fname.sendKeys(prop.getProperty("FirstNameBasic"));
		System.out.println("First Name:-" + prop.getProperty("FirstNameBasic"));
		WebElement lname = driver.findElement(By.xpath("//input[@formcontrolname='lastName']"));
		lname.sendKeys(prop.getProperty("LastNameBasic"));
		System.out.println("Last Name:-" + prop.getProperty("LastNameBasic"));
		WebElement email = driver.findElement(By.xpath("//input[@formcontrolname='email']"));
		email.sendKeys(prop.getProperty("MailIdBasic"));
		System.out.println("EmailId:-" + prop.getProperty("MailIdBasic"));
		WebElement confirmEmail = driver.findElement(By.xpath("//input[@formcontrolname='confirmEmail']"));
		confirmEmail.sendKeys(prop.getProperty("MailIdBasic"));
		System.out.println("Confrim EmailId:-" + prop.getProperty("MailIdBasic"));

		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys("1");
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys("2");
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys("3");
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys("4");
		System.out.println("Passcode:-" + "****");

		WebElement confpasscode1 = driver.findElement(By.xpath("//input[@formcontrolname='confPasscode1']"));
		confpasscode1.sendKeys("1");
		WebElement confpasscode2 = driver.findElement(By.xpath("//input[@formcontrolname='confPasscode2']"));
		confpasscode2.sendKeys("2");
		WebElement confpasscode3 = driver.findElement(By.xpath("//input[@formcontrolname='confPasscode3']"));
		confpasscode3.sendKeys("3");
		WebElement confpasscode4 = driver.findElement(By.xpath("//input[@formcontrolname='confPasscode4']"));
		confpasscode4.sendKeys("4");

		System.out.println("Confirm Passcode:-****");
		driver.findElement(By.className("globalButton")).submit();

		System.out.println("Already Have An Account");

	}

	@Test(priority = 3)
	public void successfullyLoginBasicUser() throws IOException {
		 ChromeOptions options = new ChromeOptions();
			options.addArguments("headless");
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
//		Properties prop = new Properties();
//		FileInputStream ip = new FileInputStream("./config2.properties");
//		prop.load(ip);

		// Deleting all the cookies
		driver.manage().deleteAllCookies();

		// Specifiying pageLoadTimeout and Implicit wait
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

		System.out.println("--------------------------------------------------------");
		driver.get(prop.getProperty("URL"));
		System.out.println("Url :-" + prop.getProperty("URL"));

		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailIdBasic"));
		System.out.println("Email id:-" + prop.getProperty("MailIdBasic"));

		// Thread.sleep(3000);

		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		System.out.println("Passcode:-****");

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("Successfully Login Basic User");
	}

	@Test(priority = 4, dependsOnMethods = "successfullyLoginBasicUser")
	public void successfullyLogoutBasicUser() throws InterruptedException {
		Thread.sleep(1000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		WebElement nav_btn = driver.findElement(By.xpath(
				"/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		executor.executeScript("arguments[0].click()", nav_btn);
		WebElement logout=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a/span"));
		executor.executeScript("arguments[0].click()", logout);
		System.out.println("Successfully Logout From Basic User");
		System.out.println("----------------------------------------------");
		driver.close();

	}

	@BeforeTest
	public void beforeTest() throws Exception {
		WebDriverManager.chromedriver().setup();

		file = new File("config2.properties");
		FileInputStream ip = new FileInputStream(file.getAbsolutePath());

		prop = new Properties();
		prop.load(ip);
		Class.forName("com.mysql.cj.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://104.198.155.142:3306/hexwave?createDatabaseIfNotExist=true",
				"hexwave", "h3xW@veDevAugust13");
		 ChromeOptions options = new ChromeOptions();
			options.addArguments("headless");
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
	}


	@AfterSuite
	public void afterSuite() {
		System.out.println("All Test Cases Pass");
	}

}
