package com.test.justfortest;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class dragndroptest {
	Properties prop;
	private RemoteWebDriver driver;
    private String Status = "passed";
    String username = System.getenv("LT_USERNAME") == null ? "chitrakshijaineng" : System.getenv("LT_USERNAME");
    String authkey = System.getenv("LT_ACCESS_KEY") == null ? "cnusuNbPBxkyA16xgzoOSYT4rEQBhtbgQDilvKU9GIQX3qz6JC" : System.getenv("LT_ACCESS_KEY");
    String hub = "@hub.lambdatest.com/wd/hub";
    
	@BeforeMethod
    public void setup(Method m, ITestContext ctx) throws MalformedURLException {
        

        DesiredCapabilities caps = new DesiredCapabilities();
        // Configure your capabilities here
        caps.setCapability("platform", "Windows 10");
        caps.setCapability("browserName", "Chrome");
        caps.setCapability("version", "95.0");
        caps.setCapability("resolution", "1280x1024");
        caps.setCapability("build", "TestNG With Java");
        caps.setCapability("name", m.getName() + this.getClass().getName());
        caps.setCapability("plugin", "git-testng");
        caps.setCapability("console", true);

//        String[] Tags = new String[] { "Feature", "Severe" };
//        caps.setCapability("tags", Tags);

        driver = new RemoteWebDriver(new URL("https://" + username + ":" + authkey + hub), caps);
    }


	@Test(priority = 1)
	public void succesfullyLoginAdvanceUser() throws InterruptedException, IOException {
		

		prop = new Properties();
		FileInputStream ip = new FileInputStream("./config2.properties");
		prop.load(ip);
		driver.get(prop.getProperty("URL"));
		Thread.sleep(5000);
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		Thread.sleep(5000);
	}

	@AfterMethod
	public void afterSuite() {
		System.out.println("All Test Cases Pass");
		driver.executeScript("lambda-status=" + Status);

	}
}
